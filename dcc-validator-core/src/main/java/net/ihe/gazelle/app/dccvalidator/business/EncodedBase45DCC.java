package net.ihe.gazelle.app.dccvalidator.business;

public class EncodedBase45DCC {

   private String base45string;

   public EncodedBase45DCC(String base45string) {
      setBase45string(base45string);
   }

   public String getBase45string() {
      return base45string;
   }

   public void setBase45string(String base45string) {
      if(base45string != null) {
         this.base45string = base45string;
      } else {
         throw new IllegalArgumentException("DCC Base45 string must not be null");
      }
   }
}
