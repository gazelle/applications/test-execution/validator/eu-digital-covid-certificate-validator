package net.ihe.gazelle.app.dccvalidator.adapter;

import net.ihe.gazelle.app.dccvalidator.business.DecodingException;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCBORDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncodedCOSEDCC;
import net.ihe.gazelle.app.dccvalidator.business.EncoderDecoder;
import net.ihe.gazelle.app.dccvalidator.business.EncodingException;
import net.ihe.gazelle.app.dccvalidator.business.ValidationException;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import se.digg.dgc.signatures.DGCSignatureVerifier;
import se.digg.dgc.signatures.DGCSigner;
import se.digg.dgc.signatures.impl.DefaultDGCSignatureVerifier;
import se.digg.dgc.signatures.impl.DefaultDGCSigner;

import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.List;

//FIXME Split into 2 EncoderDecoder : COSE <-> CWT, and CWT <-> CBOR
public class COSEEncoderDecoder implements EncoderDecoder<EncodedCOSEDCC, EncodedCBORDCC> {

   private static final String COSE_SIGN_REPORT = "COSE and signature Validation";
   private static final String COSE_SIGN_CONSTRAINT = "DCC must be compliant with COSE Encoding and be signed with a trusted issuer";
   private PrivateKey signerPrivateKey;
   private X509Certificate signerX509Certificate;
   private List<X509Certificate> trustedCertificates;
   private DGCSigner signer;
   private DGCSignatureVerifier signatureVerifier = new DefaultDGCSignatureVerifier();

   public COSEEncoderDecoder(PrivateKey signerPrivateKey, X509Certificate signerX509Certificate, List<X509Certificate> trustedCertificates) {
      this.signerPrivateKey = signerPrivateKey;
      this.signerX509Certificate = signerX509Certificate;
      this.trustedCertificates = trustedCertificates;
   }

   @Override
   public EncodedCOSEDCC encode(EncodedCBORDCC content) throws EncodingException {
      try {
         return new EncodedCOSEDCC(getSigner().sign(content.getCborEncoded(), content.getExpiration()));
      } catch (SignatureException | CertificateException e) {
         throw new EncodingException("Unable to sign CBOR Web Token.", e);
      }
   }

   @Override
   public EncodedCBORDCC decodeAndValidate(EncodedCOSEDCC encodedContent) throws DecodingException, ValidationException {
      try {
         List<X509Certificate> trusted = getTrustedCertificates();
         DGCSignatureVerifier.Result result = signatureVerifier.verify(encodedContent.getEncodedCOSE(), (c, k) -> trusted);
         return new EncodedCBORDCC(result.getDgcPayload(), result.getExpires());
      } catch (CertificateExpiredException | SignatureException e) {
         throw new ValidationException("Incorrect COSE element, invalid DCC", e);
      } catch (CertificateException e) {
         throw new RuntimeException("Unable to validate signature", e);
      }
   }

   @Override
   public EncodedCBORDCC decodeAndReportValidation(EncodedCOSEDCC encodedContent, final ValidationReport report) throws DecodingException {
      ValidationSubReportBuilder coseReportBuilder = new ValidationSubReportBuilder(COSE_SIGN_REPORT);
      EncodedCBORDCC cbordcc = null;
      try {
         List<X509Certificate> trusted = getTrustedCertificates();
         DGCSignatureVerifier.Result result = signatureVerifier.verify(encodedContent.getEncodedCOSE(), (c, k) -> trusted);
         cbordcc = new EncodedCBORDCC(result.getDgcPayload(), result.getExpires());
         coseReportBuilder.addPassedConstraintValidation(COSE_SIGN_CONSTRAINT, ConstraintPriority.MANDATORY);
      } catch (CertificateExpiredException | SignatureException e) {
         coseReportBuilder.addFailedConstraintValidation(COSE_SIGN_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Incorrect COSE element, invalid DCC", e);
      } catch (CertificateException e) {
         coseReportBuilder.addUndefinedConstraintValidation(COSE_SIGN_CONSTRAINT, ConstraintPriority.MANDATORY, e);
         throw new DecodingException("Unable to validate signature", e);
      } finally {
         report.addSubReport(coseReportBuilder.build());
      }
      return cbordcc;
   }

   private DGCSigner getSigner() throws CertificateException {
      if (signer == null) {
         try {
            signer = new DefaultDGCSigner(signerPrivateKey, signerX509Certificate);
         } catch (IllegalArgumentException e) {
            throw new CertificateException(e);
         }
      }
      return signer;
   }

   private List<X509Certificate> getTrustedCertificates() throws CertificateException {
      if (trustedCertificates == null) {
         throw new CertificateException("Missing trusted Certificates to verify signature");
      }
      return trustedCertificates;
   }
}
