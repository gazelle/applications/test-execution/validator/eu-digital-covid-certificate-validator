package net.ihe.gazelle.app.dccvalidator.business.torework;

public class DecodedCOSESign1DCC {

   private COSEHeader header;

   private byte[] payload;

   private byte[] signature;

   public DecodedCOSESign1DCC(byte[] payload) {
      this.payload = payload;
   }

   public byte[] getPayload() {
      return payload;
   }

   public void setPayload(byte[] payload) {
      if(payload != null) {
         this.payload = payload;
      } else {
         throw new IllegalArgumentException("Payload for COSE must not be null");
      }
   }

   public class COSEHeader {

      private byte[] protectedAttributes;
      private byte[] unprotectedAttributes;

      public COSEHeader(byte[] protectedAttributes, byte[] unprotectedAttributes) {
         this.protectedAttributes = protectedAttributes;
         this.unprotectedAttributes = unprotectedAttributes;
      }

      public byte[] getProtectedAttributes() {
         return protectedAttributes;
      }

      public void setProtectedAttributes(byte[] protectedAttributes) {
         this.protectedAttributes = protectedAttributes;
      }

      public byte[] getUnprotectedAttributes() {
         return unprotectedAttributes;
      }

      public void setUnprotectedAttributes(byte[] unprotectedAttributes) {
         this.unprotectedAttributes = unprotectedAttributes;
      }
   }

}
