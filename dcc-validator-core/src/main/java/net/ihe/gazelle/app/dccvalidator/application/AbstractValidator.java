package net.ihe.gazelle.app.dccvalidator.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import net.ihe.gazelle.modelapi.validation.business.Validator;

public abstract class AbstractValidator extends Validator {

   public AbstractValidator(String keyword) {
      super(keyword);
   }

   public AbstractValidator(String keyword, String name, String domain) {
      super(keyword, name, domain);
   }

   public abstract ValidationReport validate(byte[] object, ValidationOverview validationOverview);

   public abstract String decode(byte[] object) throws InvalidObjectException, JsonProcessingException;

}
