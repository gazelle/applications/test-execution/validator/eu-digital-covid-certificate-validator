package net.ihe.gazelle.app.dccvalidator.business.v1_3_0;

import net.ihe.gazelle.modelapi.dcc.v1_3_0.Eudcc;
import net.ihe.gazelle.modelapi.dcc.v1_3_0.PersonName;
import se.digg.dgc.transliteration.MrzEncoder;

public class EuDccBuilder {

   private static final String DCC_SCHEMA_VERSION = "1.3.0";

   private Eudcc eudcc = new Eudcc();

   public EuDccBuilder() {
      eudcc.setVer(DCC_SCHEMA_VERSION);
   }

   public EuDccBuilder setPersonName(String familyName, String givenName) {
      //FIXME call transliterator MrzEncoder through an interface
      eudcc.setNam(new PersonName(familyName, MrzEncoder.encode(familyName), givenName, MrzEncoder.encode(givenName)));
      return this;
   }

   public EuDccBuilder setDateOfBirth(Integer year, Integer month, Integer day) {
      StringBuilder dobBuilder = new StringBuilder();
      if(year != null) {
         dobBuilder.append(year);
      }
      if(month != null) {
         appendOptionalSeparator(dobBuilder).append(month);
      }
      if(day != null) {
         appendOptionalSeparator(dobBuilder).append(day);
      }
      eudcc.setDob(dobBuilder.toString());
      return this;
   }

   public Eudcc build() {
      return eudcc;
   }

   private StringBuilder appendOptionalSeparator(final StringBuilder dobBuilder) {
      if(!dobBuilder.toString().isEmpty()) {
         dobBuilder.append("-");
      }
      return dobBuilder;
   }

}
