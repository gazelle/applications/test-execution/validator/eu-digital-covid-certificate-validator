package net.ihe.gazelle.app.dccvalidator.business;

import java.util.Arrays;

public class CompressedZLibDCC {

   private byte[] content;

   public CompressedZLibDCC(byte[] content) {
      setContent(content);
   }

   public byte[] getContent() {
      return Arrays.copyOf(content, content.length);
   }

   public void setContent(byte[] content) {
      if(content != null) {
         this.content = Arrays.copyOf(content, content.length);
      } else {
         throw new IllegalArgumentException("Content for DCC Compressed CWT must not be null");
      }
   }
}
