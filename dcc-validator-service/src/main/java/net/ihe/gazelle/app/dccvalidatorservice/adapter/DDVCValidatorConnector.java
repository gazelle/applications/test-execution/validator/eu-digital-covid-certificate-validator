package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import net.ihe.gazelle.app.dccvalidator.adapter.ValidationSubReportBuilder;
import net.ihe.gazelle.app.dccvalidator.application.AbstractValidator;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationReport;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class DDVCValidatorConnector extends AbstractValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DDVCValidatorConnector.class);
    private static final String DDVC_VALIDATION_TITLE = "QR Barcode Validation From DDVC Universal Verifier";
    private static final String VERIFIED_STATUS = "VERIFIED";

    private static final String DDVC_VALIDATION_STATUS_CONSTRAINT = "The DDVC Universal Verifier must return status : \"VERIFIED\"";
    private static final String DDVC_VALIDATION_STATUS_CONSTRAINT_DETAILS = " and the reported status is : ";

    private String statusFromDDVCValidation = "";

    private String ddvcVerifierUrl;

    public DDVCValidatorConnector(String ddvcVerifierUrl) {
        super("ddvc", "DDVC Universal Verifier", "WHO");
        this.ddvcVerifierUrl = ddvcVerifierUrl;
    }


    @Override
    public ValidationReport validate(byte[] object, ValidationOverview validationOverview) {

        final ValidationReport validationReport = new ValidationReport(UUID.randomUUID().toString(), validationOverview);
        ValidationSubReportBuilder ddvcReportBuilder = new ValidationSubReportBuilder(DDVC_VALIDATION_TITLE);
        String ddvcReportAsString = getJSONReportFromDDVCValidationServer(object);
        if (ddvcReportAsString != null) {
            JSONObject ddvcReportAsJson = new JSONObject(ddvcReportAsString);
            statusFromDDVCValidation = ddvcReportAsJson.getString("status");
            if (VERIFIED_STATUS.equals(statusFromDDVCValidation)) {
                ddvcReportBuilder.addPassedConstraintValidation(DDVC_VALIDATION_STATUS_CONSTRAINT, ConstraintPriority.MANDATORY);
            } else {
                ddvcReportBuilder.addFailedConstraintValidation(DDVC_VALIDATION_STATUS_CONSTRAINT + DDVC_VALIDATION_STATUS_CONSTRAINT_DETAILS + statusFromDDVCValidation, ConstraintPriority.MANDATORY);
            }
        } else {
            ddvcReportBuilder.addFailedConstraintValidation("QR CODE cannot be decoded and sent to DDVC verifier", ConstraintPriority.MANDATORY);
        }
        validationReport.addSubReport(ddvcReportBuilder.build());
        return validationReport;
    }

    @Override
    public String decode(byte[] object) throws InvalidObjectException {
        JSONObject ddvcReportAsJson = new JSONObject(getJSONReportFromDDVCValidationServer(object));
        return ddvcReportAsJson.getString("unpacked");
    }

    private String decodeFromByteArray(byte[] object) {

        BinaryBitmap binaryBitmap;
        try {
            binaryBitmap = new BinaryBitmap(
                    new HybridBinarizer(
                            new BufferedImageLuminanceSource(ImageIO.read(
                                    new ByteArrayInputStream(object)))));
        } catch (IOException e) {
            LOGGER.error("Cannot read QR CODE", e);
            return null;
        }

        Result result;
        try {
            result = new MultiFormatReader().decode(binaryBitmap);
        } catch (NotFoundException e) {
            LOGGER.warn("Error decoding QR CODE", e);
            LOGGER.warn("Trying again with hints");
            Map<DecodeHintType, Object> tmpHintsMap = new EnumMap<>(
                    DecodeHintType.class);
            tmpHintsMap.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
            tmpHintsMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            tmpHintsMap.put(DecodeHintType.POSSIBLE_FORMATS,
                    EnumSet.allOf(BarcodeFormat.class));
            try {
                result = new MultiFormatReader().decode(binaryBitmap, tmpHintsMap);
                LOGGER.info("QR CODE decoded successfully");
            } catch (NotFoundException ex) {
                LOGGER.error(tmpHintsMap.toString());
                LOGGER.error("Error decoding QR CODE", ex);
                return null;
            }
        }

        return result.getText();
    }


    private String getJSONReportFromDDVCValidationServer(byte[] object) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            JSONObject jsonRequest = new JSONObject();
            String qrcode = decodeFromByteArray(object);
            if (qrcode == null) {
                return null;
            }
            jsonRequest.put("uri", qrcode);
            HttpPost httppost = new HttpPost(ddvcVerifierUrl);
            httppost.setEntity(new StringEntity(jsonRequest.toString(), ContentType.APPLICATION_JSON));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream inputStream = entity.getContent()) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    String result = reader.lines()
                            .collect(Collectors.joining("\n"));
                    reader.close();
                    return result;
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error contacting remote DDVC Universal Verifier Server", e);
        }
        return null;
    }
}
