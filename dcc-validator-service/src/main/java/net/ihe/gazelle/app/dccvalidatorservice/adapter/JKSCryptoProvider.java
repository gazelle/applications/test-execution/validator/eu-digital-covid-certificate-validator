package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.application.CryptoProvider;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JKSCryptoProvider implements CryptoProvider {

   private final List<X509Certificate> trustedCertificates;

   public JKSCryptoProvider(String trustStorePath, String trustStorePassword) throws KeyStoreException {
      try (InputStream inputStream = new FileInputStream(trustStorePath)){
         KeyStore truststore = KeyStore.getInstance("JKS");
         truststore.load(inputStream, trustStorePassword.toCharArray());
         PKIXParameters pkixParam = new PKIXParameters(truststore);
         trustedCertificates = pkixParam.getTrustAnchors().stream().map(TrustAnchor::getTrustedCert).collect(Collectors.toList());
      } catch (IOException | NoSuchAlgorithmException e) {
         if (e.getCause() instanceof UnrecoverableKeyException) {
            throw new KeyStoreException("Unable to load the truststore, wrong password", e);
         }
         throw new KeyStoreException("Unable to load the truststore", e);
      } catch (CertificateException e) {
         throw new KeyStoreException("Error while loading the certificates from the given truststore", e);
      } catch (InvalidAlgorithmParameterException e) {
         throw new KeyStoreException("No trusted entry in the given truststore", e);
      }
   }

   @Override
   public synchronized List<X509Certificate> getTrustedDCCIssuers() {
      return new ArrayList<>(trustedCertificates);
   }
}
