package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.application.DigitalCovidCertificateValidationService;
import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.UnkownValidatorException;
import net.ihe.gazelle.modelapi.validation.business.Validator;
import net.ihe.gazelle.modelapi.validation.interlay.ws.rest.ValidationServiceDTO;
import net.ihe.gazelle.modelapi.validation.interlay.dto.ValidationReportDTO;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@WebService(serviceName = "dccValidationService", portName = "dccValidationPort", targetNamespace = "http://dcc.validator.gazelle.ihe.net")
public class DCCValidationServiceWS implements ValidationServiceDTO {

   @Inject
   DigitalCovidCertificateValidationService validationService;

   @Override
   @WebMethod
   @WebResult(name = "name")
   public String getName() {
      return validationService.getName();
   }

   @Override
   @WebMethod(operationName = "getValidators")
   @WebResult(name = "validator")
   @XmlElementWrapper(name = "validators")
   public List<Validator> getValidators() {
      return validationService.getValidators();
   }

   @Override
   @WebMethod(operationName = "getValidatorsPerDomain")
   @WebResult(name = "validator")
   @XmlElementWrapper(name = "validators")
   public List<Validator> getValidators(@WebParam(name = "domain") String domain) {
      return validationService.getValidators(domain);
   }

   @Override
   @WebMethod
   @WebResult(name = "supportedMediaTypes")
   public List<String> getSupportedMediaTypes() {
      return validationService.getSupportedMediaTypes();
   }

   @Override
   @WebMethod
   @WebResult(name = "validationReport")
   public ValidationReportDTO validate(@WebParam(name = "object") byte[] object, @WebParam(name = "validator") String validatorKeyword)
         throws UnkownValidatorException {
      return validationService.validate(object, validatorKeyword);
   }

   @WebMethod
   @WebResult(name = "decode")
   public String decode(@WebParam(name = "object") byte[] object, @WebParam(name = "validator") String validatorKeyword ) throws UnkownValidatorException, InvalidObjectException {
      return validationService.decode(object, validatorKeyword);
   }

   private String flattenExceptionCauseMessages(Throwable t) {
      return t.getCause() != null ?
            t.getMessage() + System.lineSeparator() + flattenExceptionCauseMessages(t.getCause()) :
            t.getMessage();
   }
}
