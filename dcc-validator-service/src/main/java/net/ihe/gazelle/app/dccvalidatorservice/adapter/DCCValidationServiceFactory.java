package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.application.CryptoProvider;
import net.ihe.gazelle.app.dccvalidatorservice.application.DDVCVerifierProvider;
import net.ihe.gazelle.app.dccvalidatorservice.application.DigitalCovidCertificateValidationService;
import net.ihe.gazelle.app.dccvalidatorservice.application.ValueSetProvider;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class DCCValidationServiceFactory {

   @Inject
   CryptoProvider cryptoProvider;

   @Inject
   ValueSetProvider valueSetProvider;

   @Inject
   DDVCVerifierProvider ddvcVerifierProvider;


   @Produces
   public DigitalCovidCertificateValidationService getDCCValidationService() {
      return new DigitalCovidCertificateValidationService(cryptoProvider, valueSetProvider, ddvcVerifierProvider);
   }

}
