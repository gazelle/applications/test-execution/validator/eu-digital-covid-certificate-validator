package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidator.business.Concept;
import net.ihe.gazelle.app.dccvalidator.business.DCCValueSet;
import net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences.Preferences;
import net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences.PreferencesLoader;
import net.ihe.gazelle.app.dccvalidatorservice.application.ValueSetProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Default
@ApplicationScoped
public class DefaultValueSetProvider implements ValueSetProvider {

    private static Logger log = LoggerFactory.getLogger(DefaultValueSetProvider.class);

    @Inject
    PreferencesLoader preferencesLoader;

    @Override
    public Map<DCCValueSet, List<Concept>> getDccValueSets() {
        Map<DCCValueSet, List<Concept>> dccValueSetListMap = new HashMap<>();
        for (DCCValueSet dccValueSet : DCCValueSet.values()) {
            List<Concept> conceptList = getConceptsListFromValueSet(dccValueSet.oid, "en");
            dccValueSetListMap.put(dccValueSet, conceptList);
        }
        return dccValueSetListMap;
    }

    /**
     * <p>getConceptsListFromValueSet.</p>
     *
     * @param valueSetId a {@link java.lang.String} object.
     * @param lang a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl();
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(svsRepository).path("rest/RetrieveValueSetForSimulator");
        webTarget = webTarget.queryParam("id", valueSetId);
        if (lang != null && !lang.isEmpty()) {
            webTarget = webTarget.queryParam("lang", lang);
        }
        Response response = null;
        List<Concept> conceptsList = null;
        try {
            response = webTarget.request().get();
            if (response.getStatus() == 200) {
                String xmlContent = response.readEntity(String.class);
                Document document = XMLParser.parseStringAsDocument(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    conceptsList = new ArrayList<Concept>();
                    for (int index = 0; index < concepts.getLength(); index++) {
                        Node conceptNode = concepts.item(index);
                        NamedNodeMap attributes = conceptNode.getAttributes();
                        conceptsList.add(parseAttributes(attributes));
                    }
                    Concept.sort(conceptsList);
                }
            }
        } catch (Exception e) {
            log.error("Cannot retrieve concept list" + e.getMessage());
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return conceptsList;
    }

    private static Concept parseAttributes(NamedNodeMap attributes) {
        String code = null;
        String displayName = null;
        String codeSystem = null;
        String codeSystemName = null;
        Node attribute;
        if ((attribute = attributes.getNamedItem("code")) != null) {
            code = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("displayName")) != null) {
            displayName = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("codeSystem")) != null) {
            codeSystem = attribute.getTextContent();
        }
        if ((attribute = attributes.getNamedItem("codeSystemName")) != null) {
            codeSystemName = attribute.getTextContent();
        }
        return new Concept(code, displayName, codeSystem, codeSystemName);
    }

    /**
     *
     * @return
     */
    private String getSVSRepositoryUrl() {
        return preferencesLoader.getSvsSimulatorUrl();
    }
}
