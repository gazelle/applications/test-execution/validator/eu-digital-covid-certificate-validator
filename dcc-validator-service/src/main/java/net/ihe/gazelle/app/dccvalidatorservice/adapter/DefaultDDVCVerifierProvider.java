package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences.PreferencesLoader;
import net.ihe.gazelle.app.dccvalidatorservice.application.DDVCVerifierProvider;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

@Default
@ApplicationScoped
public class DefaultDDVCVerifierProvider implements DDVCVerifierProvider {

    @Inject
    PreferencesLoader preferencesLoader;

    @Override
    public String getDDVCVerifierUrl() {
        return preferencesLoader.getDdvcVerifierUrl();
    }
}
