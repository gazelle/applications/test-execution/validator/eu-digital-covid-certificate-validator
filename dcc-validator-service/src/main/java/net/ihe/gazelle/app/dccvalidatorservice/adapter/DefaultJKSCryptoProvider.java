package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.adapter.preferences.PreferencesLoader;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.security.KeyStoreException;

@Default
@ApplicationScoped
public class DefaultJKSCryptoProvider {

    @Inject
    PreferencesLoader preferencesLoader;


    @Produces
    public JKSCryptoProvider defaultJKSCryptoProvider() throws KeyStoreException {
        return new JKSCryptoProvider(preferencesLoader.getTruststorePath(), preferencesLoader.getTruststorePassword());
    }
}
