package net.ihe.gazelle.app.dccvalidatorservice.application;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class DDVCBarcodeTestProvider {

    private static final String DDVC_TESTDATA_RESOURCE_DIR = "ddvc";
    private static final String BARCODE_IMAGE_EXTENSION = ".png";


    public String testDataName;

    public DDVCBarcodeTestProvider(String testDataName) {
        this.testDataName = testDataName;
    }

    public byte[] getDDVCBarcodeImage() throws IOException {
        return getResourceAsStream(buildDDVCResourcePath(testDataName, BARCODE_IMAGE_EXTENSION)).readAllBytes();
    }

    public String getDDVCBase64BarcodeImage() throws IOException {
        return Base64.getEncoder().encodeToString(getDDVCBarcodeImage());
    }


    private String buildDDVCResourcePath(String fileName, String fileExtension) {
        return DDVC_TESTDATA_RESOURCE_DIR + File.separator + fileName + fileExtension;
    }

    private InputStream getResourceAsStream(String ressourcePath) {
        return DDVCBarcodeTestProvider.class.getClassLoader().getResourceAsStream(ressourcePath);
    }


}
