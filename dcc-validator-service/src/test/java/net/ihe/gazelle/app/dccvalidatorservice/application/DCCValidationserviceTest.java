package net.ihe.gazelle.app.dccvalidatorservice.application;

import net.ihe.gazelle.app.dccvalidatorservice.adapter.DDVCVerifierProviderForUt;
import net.ihe.gazelle.app.dccvalidatorservice.adapter.JKSCryptoProvider;
import net.ihe.gazelle.app.dccvalidatorservice.adapter.ValueSetProviderForUT;
import net.ihe.gazelle.modelapi.validation.business.UnkownValidatorException;
import net.ihe.gazelle.modelapi.validation.business.Validator;
import net.ihe.gazelle.modelapi.validation.interlay.dto.ValidationReportDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DCCValidationserviceTest {

   DigitalCovidCertificateValidationService validationService;
   List<String> exceptedListOfValidators = new ArrayList<>();


   @BeforeEach
   public void setup() throws KeyStoreException {
      validationService = new DigitalCovidCertificateValidationService(new JKSCryptoProvider(
            DCCValidationserviceTest.class.getClassLoader().getResource("keystore.jks").getPath(), "password"
      ), new ValueSetProviderForUT(), new DDVCVerifierProviderForUt());
      exceptedListOfValidators.add("basic 1.0.0");
      exceptedListOfValidators.add("ddvc");

   }

   @Test
   public void testGetValidator() {
      List<Validator> listOfValidators = validationService.getValidators();
      List<String> actualListOfValidator = new  ArrayList<>();
      for (Validator validator : listOfValidators) {
            actualListOfValidator.add(validator.getKeyword());
      }
      List<String> sortedExceptedListOfValidators = exceptedListOfValidators.stream().sorted().collect(Collectors.toList());
      List<String> sortedActualListOfValidators = actualListOfValidator.stream().sorted().collect(Collectors.toList());
      Assertions.assertEquals(sortedExceptedListOfValidators,sortedActualListOfValidators);
   }
//FIXME Signed DCC are expired and must be updated.
   @Disabled("Test validateTest(String testDataName) is disabled because Signed DCC are expired and must be updated")
   @ParameterizedTest
   @ValueSource(strings = { "fr_recovery_ok", "fr_vaccin_ok" })
   public void validateTest(String testDataName) {
      JSONTestDataProvider testDataProvider = new JSONTestDataProvider(testDataName);
      validationService = new DigitalCovidCertificateValidationService(testDataProvider, new ValueSetProviderForUT(), new DDVCVerifierProviderForUt());
      Assertions.assertDoesNotThrow(() -> validationService.decode(testDataProvider.getBarcodeImage(), "basic 1.0.0"));
   }


   @ParameterizedTest
   @ValueSource(strings = { "fr_test-pcr_ok" })
   public void validateTestExpired(String testDataName) {
      JSONTestDataProvider testDataProvider = new JSONTestDataProvider(testDataName);
      validationService = new DigitalCovidCertificateValidationService(testDataProvider, new ValueSetProviderForUT(), new DDVCVerifierProviderForUt());
      Assertions.assertThrows(RuntimeException.class, () -> validationService.decode(testDataProvider.getBarcodeImage(), "basic 1.0.0"));
   }

   @Test
   public void unknownValidatorTest() {
      Assertions.assertThrows(UnkownValidatorException.class, () -> validationService.decode(null, "unknown validator"));
   }

   @Disabled("Test validateValidDDVCDoesNotThrowTest is disabled because the DDVC MAY be not available")
   @ParameterizedTest
   @ValueSource(strings = { "valid_verified" })
   public void validateValidDDVCDoesNotThrowTest(String testDataName) throws IOException, UnkownValidatorException {
      DDVCBarcodeTestProvider testDataProvider = new DDVCBarcodeTestProvider(testDataName);
      ValidationReportDTO validationReportDTO = validationService.validate(testDataProvider.getDDVCBarcodeImage(), "ddvc");
      Assertions.assertDoesNotThrow(() -> validationService.validate(testDataProvider.getDDVCBarcodeImage(), "ddvc"));
   }

   @Disabled("Test validateValidDDVCTest is disabled because the DDVC MAY be not available")
   @ParameterizedTest
   @ValueSource(strings = { "valid_verified" })
   public void validateValidDDVCTest(String testDataName) throws IOException, UnkownValidatorException {
      DDVCBarcodeTestProvider testDataProvider = new DDVCBarcodeTestProvider(testDataName);
      ValidationReportDTO validationReportDTO = validationService.validate(testDataProvider.getDDVCBarcodeImage(), "ddvc");
      Assertions.assertEquals("PASSED",validationReportDTO.getSubReports().get(0).getConstraints().get(0).getTestResult().toString());
   }

   @Disabled("Test validateInvalidNotTrustedIssuerDDVCTest is disabled because the DDVC MAY be not available")
   @ParameterizedTest
   @ValueSource(strings = { "invalid_issuer_not_trusted" })
   public void validateInvalidNotTrustedIssuerDDVCTest(String testDataName) throws IOException, UnkownValidatorException {
      DDVCBarcodeTestProvider testDataProvider = new DDVCBarcodeTestProvider(testDataName);
      ValidationReportDTO validationReportDTO = validationService.validate(testDataProvider.getDDVCBarcodeImage(), "ddvc");
      Assertions.assertEquals("FAILED",validationReportDTO.getSubReports().get(0).getConstraints().get(0).getTestResult().toString());
   }

}
