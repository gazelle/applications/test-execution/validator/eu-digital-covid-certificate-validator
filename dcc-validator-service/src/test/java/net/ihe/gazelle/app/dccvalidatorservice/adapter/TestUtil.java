package net.ihe.gazelle.app.dccvalidatorservice.adapter;

import net.ihe.gazelle.app.dccvalidatorservice.application.JSONTestDataProvider;

import java.io.IOException;

public class TestUtil {

   public static void main(final String[] args) {
      try {
         System.out.println(new JSONTestDataProvider("fr_recovery_ok").getBase64BarcodeImage());
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

}
